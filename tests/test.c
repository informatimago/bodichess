#include <stdarg.h>
#include <stdio.h>
#include "test.h"

static test current_test;
static const char* current_testcase;

test* test_init(void){
    current_test.successes=0;
    current_test.failures=0;
    return &current_test;}

void testcase(const char* name){
    current_testcase=name;}

void success(const char* format, ...){
    current_test.successes++;
    printf("Test case %-20s SUCCESS\n",current_testcase);
    if(format){
        va_list args;
        va_start(args,format);
        vprintf(format,args);
        va_end(args);
        printf("\n");}}

void failure(const char* format, ...){
    current_test.failures++;
    printf("Test case %-20s FAILURE\n",current_testcase);
    if(format){
        va_list args;
        va_start(args,format);
        vprintf(format,args);
        va_end(args);
        printf("\n");}}

void report(){
    printf("Successes:     %6d\n",current_test.successes);
    printf("Failures:  :   %6d\n",current_test.failures);
    printf("Total tests:   %6d\n",current_test.successes+current_test.failures);}


