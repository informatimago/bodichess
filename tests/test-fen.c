#include <fen.h>

#include "test.h"

bool piece_list_equal(struct list_head* a,struct list_head* b){
    /* TODO: implement piece_list_equal() */
    return true;}

bool board_equal(board_t* a,board_t* b){
    for(int i=0;i<8*8*2;i++){
        if((*a)[i].piece!=(*b)[i].piece){
            return false;}}
    return true;}

bool position_equal(pos_t* a,pos_t* b){
    return (a->turn==b->turn)
            &&(a->castle==b->castle)
            &&(a->en_passant==b->en_passant)
            &&(a->clock_50==b->clock_50)
            &&(a->curmove==b->curmove)
            && board_equal(a->board,b->board)
            && piece_list_equal(&a->w_pieces,&b->w_pieces)
            && piece_list_equal(&a->b_pieces,&b->b_pieces);}

void test_fen(){
    {
        testcase("fen initial position");
        pos_t* pos=pos_create();
        pos_t* res=fen2pos(pos,"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        if(position_equal(pos,res)){
            success("fen initial position valid");
        }else{
            failure("fen initial position invalid");}
    }

    {
        testcase("fen two kings");
        pos_t* pos=pos_create();
        pos_t* res=fen2pos(pos,"k7/8/8/8/8/8/8/7K w KQkq - 0 1");
        if(false /* TODO: implement this! */ ){
            success("fen two kings position valid");
        }else{
            failure("fen two kings position invalid");}
    }
    
    /* etc */
}
    
int main(){
    test* test=test_init();
    test_fen();
    report();
    return (test->failures==0)?0:1;}
