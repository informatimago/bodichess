#ifndef test_h
#define test_h

typedef struct {
    int successes;
    int failures;
} test;

test* test_init(void);
void testcase(const char* name);
void success(const char* format, ...);
void failure(const char* format, ...);
void report(void);

#endif
